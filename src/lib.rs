use futures::join;
use futures::task::{ArcWake, Waker, waker_ref};
use futures::future::{BoxFuture, FutureExt};
use std::time::{Duration, Instant};
use std::future::Future;
use std::task::{Context, Poll};
use std::pin::Pin;
use std::sync::Arc;
use std::sync::Mutex;
use std::cmp::Reverse;
use std::thread;
use std::cmp::Ordering;

struct CoSleepFutureState {
    ready: bool,
    waker: Option<Waker>,
}
struct CoSleepFuture {
    end: Instant,
    s: Arc<Mutex<CoSleepFutureState>>,
}

impl Future for CoSleepFuture {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut s = self.s.lock().unwrap();
        if s.ready {
            Poll::Ready(())
        } else {
            if let None = s.waker {
                s.waker = Some(cx.waker().clone());
            }
            Poll::Pending
        }
    }
}

fn co_sleep(s: u64) -> CoSleepFuture {
    let now = Instant::now();
    let fut = CoSleepFuture {
        end: now + Duration::new(s, 0),
        s: Arc::new(Mutex::new(CoSleepFutureState {
            ready: false,
            waker: None,
        })),
    };
    let state = fut.s.clone();

    thread::spawn(move || {
        std::thread::sleep(Duration::new(s, 0));

        let mut s = state.lock().unwrap();
        s.ready = true;
        println!("Timer elapsed!");
        if let Some(waker) = &s.waker {
            waker.wake_by_ref();
        }
    });

    fut
}

async fn blubb(s: u64) {
    println!("blubb: start");
    co_sleep(s).await;
    //async_std::task::sleep(Duration::new(s, 0)).await;
    println!("blubb: end");
}

pub async extern "C" fn async_co_main() {
    println!("Hello, world!");

    let fut1 = blubb(5);
    let fut2 = blubb(2);

    join!(fut1, fut2);
}

#[no_mangle]
pub extern "C" fn co_main() -> *mut Pin<Box<dyn Future<Output = ()>>> {
    let fut = async_co_main().boxed();
    unsafe { Box::into_raw(Box::new(fut)) }
}

#[no_mangle]
extern {
    fn qemu_coroutine_self() -> usize;
    fn aio_co_wake(co: usize);
}

struct MainloopWaker {
    co: usize,
}
impl ArcWake for MainloopWaker {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        println!("Wake!");
        unsafe { aio_co_wake(arc_self.co) };
    }
}

#[no_mangle]
pub extern "C" fn poll_future(f_ptr: *mut Pin<Box<dyn Future<Output=()>>>) -> bool {
    let mut f = unsafe { Box::from_raw(f_ptr) };
    let mut mwaker = Arc::new(MainloopWaker {
        co: unsafe { qemu_coroutine_self() },
    });
    let waker = waker_ref(&mwaker);
    let mut cx = Context::from_waker(&waker);

    if let Poll::Ready(_) = (*f).as_mut().poll(&mut cx) {
        println!("Future completed!");
        return false;
    }

    Box::leak(f);
    true
}

#[no_mangle]
pub extern "C" fn rust_hello() -> u32 {
    println!("Hello World from Rust!");
    return 42;
}
